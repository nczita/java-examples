package pl.saltsoft;

import com.google.gson.Gson;

public class JsonSerialization {

    public String beerSerialize(Beer beer){
        Gson gson = new Gson();
        String json = gson.toJson(beer);
        return json;
    }

    public Beer beerDeserialize(String json) {
        Gson gson = new Gson();
        Beer beer = gson.fromJson(json, Beer.class);
        return beer;
    }
}
