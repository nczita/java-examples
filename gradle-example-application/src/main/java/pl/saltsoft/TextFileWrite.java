package pl.saltsoft;

import java.io.*;

public class TextFileWrite {
    void writeFile(String filePath) throws Exception {
        File file = new File(filePath);
        FileWriter w = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(w);

        bw.write("Hello World");
        bw.newLine();
        bw.flush();
        bw.close();
    }
}
