package pl.saltsoft;

public class Beer {
    public String name;
    public double alcohol;
    public int ibu;

    public Beer(String name, double alcohol, int ibu) {
        this.name = name;
        this.alcohol = alcohol;
        this.ibu = ibu;
    }
}